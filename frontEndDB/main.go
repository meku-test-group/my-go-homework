package main

import (
	"fmt"
	"log"
	"net/http"
	"strconv"
)

type dollars float32

func (d dollars) String() string {
	return fmt.Sprintf("$%.2f", d)
}

type database map[string]dollars

////////////////////// Adding Handlers  //////////////////

func (db database) list(w http.ResponseWriter, req *http.Request) {
	for item, price := range db {
		fmt.Fprintf(w, "%s: %s\n", item, price)
	}
}

func (db database) add(w http.ResponseWriter, req *http.Request) {
	item := req.URL.Query().Get("item")
	price := req.URL.Query().Get("price")

	if _, ok := db[item]; ok {
		msg := fmt.Sprintf("Duplicate Item: %q\n", item)
		http.Error(w, msg, http.StatusBadRequest) // 400
		return
	}

	p, err := strconv.ParseFloat(price, 32)
	if err != nil {
		msg := fmt.Sprintf("Invalid Price: %q\n", price)
		http.Error(w, msg, http.StatusBadRequest) // 400
		return
	}

	db[item] = dollars(p)

	fmt.Fprintf(w, "Added %s with Price of %s", item, db[item])
}

func (db database) update(w http.ResponseWriter, req *http.Request) {
	item := req.URL.Query().Get("item")
	price := req.URL.Query().Get("price")

	if _, ok := db[item]; !ok {
		msg := fmt.Sprintf("Item NOT Found: %q\n", item)
		http.Error(w, msg, http.StatusNotFound) // 400
		return
	}

	p, err := strconv.ParseFloat(price, 32)
	if err != nil {
		msg := fmt.Sprintf("Invalid Price: %q\n", price)
		http.Error(w, msg, http.StatusBadRequest) // 400
		return
	}

	db[item] = dollars(p)

	fmt.Fprintf(w, "New Price For %s is %s", db[item], price)
}

func (db database) fetch(w http.ResponseWriter, req *http.Request) {
	item := req.URL.Query().Get("item")

	if _, ok := db[item]; !ok {
		msg := fmt.Sprintf("Item NOT Found: %q\n", item)
		http.Error(w, msg, http.StatusNotFound) // 400
		return
	}

	fmt.Fprintf(w, "Item %s has a price %s", item, db[item])
}

func (db database) drop(w http.ResponseWriter, req *http.Request) {
	item := req.URL.Query().Get("item")

	if _, ok := db[item]; !ok {
		msg := fmt.Sprintf("Item NOT Found: %q\n", item)
		http.Error(w, msg, http.StatusNotFound) // 400
		return
	}

	delete(db, item)
	fmt.Fprintf(w, "Item %s has a price %s", item, db[item])
}

func main() {
	db := database{
		"shoes": 50,
		"socks": 5,
	}

	////////////////////// Adding Routes  //////////////////

	http.HandleFunc("/list", db.list)
	http.HandleFunc("/create", db.add)
	http.HandleFunc("/update", db.update)
	http.HandleFunc("/read", db.fetch)
	http.HandleFunc("/delete", db.drop)

	log.Fatal(http.ListenAndServe("localhost:8080", nil))
}
